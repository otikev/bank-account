package com.tala.bank;

import com.tala.bank.utils.HttpHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Created by kevin on 9/2/2016.
 */
public abstract class BaseIntegrationTest extends AbstractTestNGSpringContextTests {

    protected HttpHelper httpHelper;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    protected void setupHttpHelper(int port){
        String baseUrl = "http://localhost:" + port;
        httpHelper = new HttpHelper(baseUrl);
    }
}
