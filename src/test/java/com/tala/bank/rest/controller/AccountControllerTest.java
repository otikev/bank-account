package com.tala.bank.rest.controller;

import com.tala.bank.BaseIntegrationTest;
import com.tala.bank.model.Deposit;
import org.joda.time.LocalDate;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

/**
 * Created by kevin on 9/1/2016.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountControllerTest extends BaseIntegrationTest {

    @LocalServerPort
    int port;

    @BeforeMethod
    public void setUp() throws Exception {
        setupHttpHelper(this.port);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        jdbcTemplate.execute("delete from deposit");
        jdbcTemplate.execute("delete from withdrawal");
        jdbcTemplate.execute("update account set balance = 0 where id = 1");
    }

    @Test
    public void testBalanceDisplaysCorrectAmount() throws Exception {
        //set data
        jdbcTemplate.execute("update account set balance = 30000 where id = 1");

        JSONObject response = httpHelper.sendGet("/rest/account/balance");
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",true);
        expectedJson.put("payload","$300.00");

        JSONAssert.assertEquals(expectedJson,response,true);

    }

    @Test
    public void testDepositSuccess() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",1000);
        JSONObject response = httpHelper.sendPost("/rest/account/deposit",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",true);
        expectedJson.put("payload","Transaction successful");

        JSONAssert.assertEquals(expectedJson,response,true);

        response = httpHelper.sendGet("/rest/account/balance");
        System.out.println(response);

        expectedJson = new JSONObject();
        expectedJson.put("success",true);
        expectedJson.put("payload","$1000.00");

        JSONAssert.assertEquals(expectedJson,response,true);

    }

    @Test
    public void testDepositFailsIfMaxDailyLimitReached() throws Exception {
        //set deposits to 150k, each deposit not more than 40k
        String sql = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql,new Object[]{1,4000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,4000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,4000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,3000000,new LocalDate().toDate()});
        jdbcTemplate.update("update account set balance = 15000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",1000);
        JSONObject response = httpHelper.sendPost("/rest/account/deposit",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Amount exceeds daily deposit limit");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testDepositFailsIfAmountWillExceedDailyLimit() throws Exception {
        //set deposits to 120k, each deposit not more than 40k
        String sql = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql,new Object[]{1,4000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,4000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,4000000,new LocalDate().toDate()});
        jdbcTemplate.update("update account set balance = 12000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",40000);
        JSONObject response = httpHelper.sendPost("/rest/account/deposit",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Amount exceeds daily deposit limit");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testDepositFailsIfAmountIsMoreThan40k() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",50000);
        JSONObject response = httpHelper.sendPost("/rest/account/deposit",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Amount exceeds transaction deposit limit");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testDepositFailsAfterFourTransactionsForTheDay() throws Exception {
        String sql = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update("update account set balance = 8000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",40000);
        JSONObject response = httpHelper.sendPost("/rest/account/deposit",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Exceeded daily deposit frequency");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testWithdrawSuccess() throws Exception {
        String sql1 = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});

        jdbcTemplate.update("update account set balance = 2000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",1000);
        JSONObject response = httpHelper.sendPost("/rest/account/withdrawal",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",true);
        expectedJson.put("payload","Transaction successful");

        JSONAssert.assertEquals(expectedJson,response,true);

        response = httpHelper.sendGet("/rest/account/balance");
        System.out.println(response);

        expectedJson = new JSONObject();
        expectedJson.put("success",true);
        expectedJson.put("payload","$19000.00");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testWithdrawFailsWhenDailyLimitReached() throws Exception {
        String sql1 = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});


        String sql = "insert into withdrawal (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,3000000,new LocalDate().toDate()});

        jdbcTemplate.update("update account set balance = 3000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",500);
        JSONObject response = httpHelper.sendPost("/rest/account/withdrawal",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Amount exceeds daily withdrawal limit");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testWithdrawFailsWhenTransactionLimitReached() throws Exception {
        String sql1 = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});

        jdbcTemplate.update("update account set balance = 8000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",20000.10);
        JSONObject response = httpHelper.sendPost("/rest/account/withdrawal",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Amount exceeds transaction withdrawal limit");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testWithdrawFailsAfterThreeTransactions() throws Exception {
        String sql1 = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql1,new Object[]{1,2000000,new LocalDate().toDate()});


        String sql = "insert into withdrawal (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql,new Object[]{1,1000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,1000000,new LocalDate().toDate()});
        jdbcTemplate.update(sql,new Object[]{1,1000000,new LocalDate().toDate()});

        jdbcTemplate.update("update account set balance = 5000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",500);
        JSONObject response = httpHelper.sendPost("/rest/account/withdrawal",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Exceeded daily withdrawal frequency");

        JSONAssert.assertEquals(expectedJson,response,true);
    }

    @Test
    public void testWithdrawFailsWhenAmountIsMoreThanBalance() throws Exception {
        String sql1 = "insert into deposit (account_id,amount,date) values (?,?,?)";
        jdbcTemplate.update(sql1,new Object[]{1,1000000,new LocalDate().toDate()});

        jdbcTemplate.update("update account set balance = 1000000 where id = 1");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("amount",11000);
        JSONObject response = httpHelper.sendPost("/rest/account/withdrawal",jsonObject);
        System.out.println(response);

        JSONObject expectedJson = new JSONObject();
        expectedJson.put("success",false);
        expectedJson.put("error","Balance is less than withdrawal amount");

        JSONAssert.assertEquals(expectedJson,response,true);
    }
}