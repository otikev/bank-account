package com.tala.bank.model;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by kevin on 8/31/2016.
 */
@Entity
public class Withdrawal {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    Long amount;

    Date date;

    Long accountId;

    private Withdrawal(){}

    public Withdrawal(Long accountId,Long amount) {
        this.accountId=accountId;
        this.amount = amount;
        this.date = LocalDate.now().toDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
}
