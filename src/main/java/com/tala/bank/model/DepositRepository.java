package com.tala.bank.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by kevin on 8/31/2016.
 */
public interface DepositRepository extends CrudRepository<Deposit,Long> {

    @Query(value = "select * from deposit where account_id = ?1 and date = ?2", nativeQuery = true)
    List<Deposit> findByAccountAndDate(Long accountId,Date date);
}
