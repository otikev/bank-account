package com.tala.bank.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by kevin on 8/31/2016.
 */
public interface AccountRepository extends CrudRepository<Account,Long> {
}
