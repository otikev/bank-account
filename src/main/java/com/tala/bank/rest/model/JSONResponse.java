package com.tala.bank.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by kevin on 8/31/2016.
 */
public class JSONResponse {

    private boolean success;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object payload;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String error;

    public static JSONResponse successResponse(Object _payload){
        if(_payload == null){
            throw new RuntimeException("There must be a value for payload");
        }
        JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(true);
        jsonResponse.setPayload(_payload);
        jsonResponse.setError(null);
        return jsonResponse;
    }

    public static JSONResponse failureResponse(String error){
        if(error == null || error.length()==0){
            throw new RuntimeException("There must be a value for error");
        }
        JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setPayload(null);
        jsonResponse.setError(error);
        return jsonResponse;
    }

    private JSONResponse(){}

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
