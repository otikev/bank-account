package com.tala.bank.rest.controller;

import com.tala.bank.rest.model.JSONResponse;
import com.tala.bank.service.AccountService;
import com.tala.bank.utils.CurrencyHelper;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by kevin on 8/31/2016.
 */
@RequestMapping("rest/account")
@RestController
public class AccountController {
    static final String BALANCE="/balance",DEPOSIT="/deposit", WITHDRAWAL="/withdrawal";

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    AccountService accountService;

    @RequestMapping(value = BALANCE, method = RequestMethod.GET)
    public ResponseEntity<JSONResponse> balance() {
        try{
            Long balanceInCents = accountService.getAccount().getBalance();
            BigDecimal balance = CurrencyHelper.centsToCurrency(balanceInCents);

            return new ResponseEntity<JSONResponse>(JSONResponse.successResponse("$" + balance), HttpStatus.OK);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return new ResponseEntity<JSONResponse>(JSONResponse.failureResponse(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()), HttpStatus.OK);
        }
    }

    @RequestMapping(value = DEPOSIT, method = RequestMethod.POST)
    public ResponseEntity<JSONResponse> deposit(@RequestBody String body) {
        try{
            JSONObject jsonObject = new JSONObject(body);
            Object obj = jsonObject.get("amount");
            String amount = "";
            if(obj instanceof String){
                amount = String.valueOf(obj);
            }else{
                amount = String.valueOf(jsonObject.getDouble("amount"));
            }

            Long cents = CurrencyHelper.currencyToCents(new BigDecimal(amount));
            Map<String, Object> response = accountService.deposit(cents);
            if((Boolean)response.get("success")){
                return new ResponseEntity<JSONResponse>(JSONResponse.successResponse(response.get("message")), HttpStatus.OK);
            }
            return new ResponseEntity<JSONResponse>(JSONResponse.failureResponse((String)response.get("message")), HttpStatus.OK);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return new ResponseEntity<JSONResponse>(JSONResponse.failureResponse(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()), HttpStatus.OK);
        }
    }

    @RequestMapping(value = WITHDRAWAL, method = RequestMethod.POST)
    public ResponseEntity<JSONResponse> withdraw(@RequestBody String body) {
        try{
            JSONObject jsonObject = new JSONObject(body);
            Object obj = jsonObject.get("amount");
            String amount = "";
            if(obj instanceof String){
                amount = String.valueOf(obj);
            }else{
                amount = String.valueOf(jsonObject.getDouble("amount"));
            }

            Long cents = CurrencyHelper.currencyToCents(new BigDecimal(amount));
            Map<String, Object> response = accountService.withdraw(cents);
            if((Boolean)response.get("success")){
                return new ResponseEntity<JSONResponse>(JSONResponse.successResponse(response.get("message")), HttpStatus.OK);
            }
            return new ResponseEntity<JSONResponse>(JSONResponse.failureResponse((String)response.get("message")), HttpStatus.OK);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return new ResponseEntity<JSONResponse>(JSONResponse.failureResponse(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()), HttpStatus.OK);
        }
    }
}
