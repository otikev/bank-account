package com.tala.bank.service;

import com.tala.bank.model.Account;
import com.tala.bank.model.AccountRepository;
import com.tala.bank.model.Deposit;
import com.tala.bank.model.Withdrawal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.tala.bank.utils.Constants.*;

/**
 * Created by kevin on 8/31/2016.
 */
@Service("accountService")
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    DepositService depositService;

    @Autowired
    WithdrawalService withdrawalService;

    public Map<String, Object> deposit(Long depositInCents) {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("success",true);
        response.put("message","Transaction successful");

        Account account = getAccount();
        Long depositAmountToday = depositService.totalDepositAmountToday(account);
        Long expectedDeposit = depositAmountToday+depositInCents;
        if(expectedDeposit.compareTo(MAX_DAILY_DEPOSIT_CENTS) > 0 ){
            response.put("success",false);
            response.put("message","Amount exceeds daily deposit limit");
            return response;
        }

        if(depositInCents.compareTo(MAX_TRANSACTION_DEPOSIT_CENTS) > 0){
            response.put("success",false);
            response.put("message","Amount exceeds transaction deposit limit");
            return response;
        }

        int depositCountToday = depositService.totalTransactionsToday(account);
        if(depositCountToday >= MAX_DAILY_DEPOSIT_FREQUENCY){
            response.put("success",false);
            response.put("message","Exceeded daily deposit frequency");
            return response;
        }
        depositService.deposit(account,depositInCents);
        return response;
    }

    public Map<String, Object> withdraw(Long amountInCents) {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("success",true);
        response.put("message","Transaction successful");

        Account account = getAccount();

        Long totalWithdrawalToday = withdrawalService.totalWithdrawalAmountToday(account);
        Long expectedWithdrawal = totalWithdrawalToday+amountInCents;
        if(expectedWithdrawal.compareTo(MAX_DAILY_WITHDRAWAL_CENTS) > 0){
            response.put("success",false);
            response.put("message","Amount exceeds daily withdrawal limit");
            return response;
        }

        if(amountInCents.compareTo(MAX_TRANSACTION_WITHDRAWAL_CENTS) > 0){
            response.put("success",false);
            response.put("message","Amount exceeds transaction withdrawal limit");
            return response;
        }

        int withdrawalCountToday = withdrawalService.totalTransactionsToday(account);
        if(withdrawalCountToday >= MAX_DAILY_WITHDRAWAL_FREQUENCY){
            response.put("success",false);
            response.put("message","Exceeded daily withdrawal frequency");
            return response;
        }

        Long balance = account.getBalance();
        if(amountInCents.compareTo(balance) > 0){
            response.put("success",false);
            response.put("message","Balance is less than withdrawal amount");
            return response;
        }

        withdrawalService.withdraw(account,amountInCents);
        return response;//success
    }

    public Account getAccount() {
        return accountRepository.findOne(1L);
    }

    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }
}
