package com.tala.bank.service;

import com.tala.bank.model.Account;
import com.tala.bank.model.Deposit;

/**
 * Created by kevin on 8/31/2016.
 */
public interface DepositService {
    Deposit deposit(Account account, Long amountInCents);
    Long totalDepositAmountToday(Account account);
    Integer totalTransactionsToday(Account account);
}
