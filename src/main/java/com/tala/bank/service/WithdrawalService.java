package com.tala.bank.service;

import com.tala.bank.model.Account;
import com.tala.bank.model.Withdrawal;

/**
 * Created by kevin on 8/31/2016.
 */
public interface WithdrawalService {
    Withdrawal withdraw(Account account, Long amountInCents);
    Long totalWithdrawalAmountToday(Account account);
    Integer totalTransactionsToday(Account account);
}
