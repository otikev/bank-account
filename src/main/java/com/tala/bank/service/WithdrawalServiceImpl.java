package com.tala.bank.service;

import com.tala.bank.model.Account;
import com.tala.bank.model.Withdrawal;
import com.tala.bank.model.WithdrawalRepository;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kevin on 8/31/2016.
 */
@Service("withdrawalService")
@Transactional
public class WithdrawalServiceImpl implements WithdrawalService {

    @Autowired
    WithdrawalRepository withdrawalRepository;

    @Autowired
    AccountService accountService;

    public Withdrawal withdraw(Account account, Long amountInCents) {
        Long bal = account.getBalance();
        account.setBalance(bal-amountInCents);
        accountService.saveAccount(account);
        Withdrawal withdrawal = new Withdrawal(account.getId(),amountInCents);
        return withdrawalRepository.save(withdrawal);
    }

    public Long totalWithdrawalAmountToday(Account account) {
        //TODO: use sql SUM() function instead of this loop
        List<Withdrawal> withdrawals = withdrawalRepository.findByAccountAndDate(account.getId(),LocalDate.now().toDate());
        long total=0;
        for(Withdrawal withdrawal : withdrawals){
            total += withdrawal.getAmount();
        }
        return total;
    }

    public Integer totalTransactionsToday(Account account) {
        List<Withdrawal> withdrawals = withdrawalRepository.findByAccountAndDate(account.getId(),LocalDate.now().toDate());
        return withdrawals.size();
    }
}
