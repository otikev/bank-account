package com.tala.bank.service;

import com.tala.bank.model.Account;
import com.tala.bank.model.Deposit;
import com.tala.bank.model.Withdrawal;

import java.util.Map;

/**
 * Created by kevin on 8/31/2016.
 */
public interface AccountService {
    Map<String,Object> deposit(Long depositInCents);
    Map<String,Object> withdraw(Long amountInCents);
    Account getAccount();
    Account saveAccount(Account account);
}
