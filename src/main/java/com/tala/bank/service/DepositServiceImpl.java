package com.tala.bank.service;

import com.tala.bank.model.Account;
import com.tala.bank.model.Deposit;
import com.tala.bank.model.DepositRepository;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kevin on 8/31/2016.
 */
@Service("depositService")
@Transactional
public class DepositServiceImpl implements DepositService {

    @Autowired
    DepositRepository depositRepository;

    @Autowired
    AccountService accountService;

    public Deposit deposit(Account account, Long amountInCents) {
        Long bal = account.getBalance();
        account.setBalance(bal+amountInCents);
        accountService.saveAccount(account);
        Deposit deposit = new Deposit(account.getId(),amountInCents);
        return depositRepository.save(deposit);
    }

    public Long totalDepositAmountToday(Account account) {
        //TODO: use sql SUM() function instead of this loop
        List<Deposit> deposits = depositRepository.findByAccountAndDate(account.getId(),LocalDate.now().toDate());
        long total=0;
        for(Deposit deposit : deposits){
            total += deposit.getAmount();
        }
        return total;
    }

    public Integer totalTransactionsToday(Account account) {
        List<Deposit> deposits = depositRepository.findByAccountAndDate(account.getId(),LocalDate.now().toDate());
        return deposits.size();
    }
}
