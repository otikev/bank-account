package com.tala.bank.utils;

import java.math.BigDecimal;

/**
 * Created by kevin on 9/1/2016.
 */
public final class CurrencyHelper {

    private static final BigDecimal HUNDRED = new BigDecimal("100");

    public static Long currencyToCents(BigDecimal amount){
        return amount.multiply(HUNDRED).longValue();
    }

    public static BigDecimal centsToCurrency(Long cents){
        return new BigDecimal(cents).movePointLeft(2);
    }
}
