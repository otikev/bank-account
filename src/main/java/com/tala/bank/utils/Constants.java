package com.tala.bank.utils;

/**
 * Created by kevin on 9/1/2016.
 */
public class Constants {

    public static final Long MAX_DAILY_WITHDRAWAL_CENTS = 5000000L;
    public static final Long MAX_TRANSACTION_WITHDRAWAL_CENTS = 2000000L;
    public static final int MAX_DAILY_WITHDRAWAL_FREQUENCY = 3;

    public static final Long MAX_DAILY_DEPOSIT_CENTS = 15000000L;
    public static final Long MAX_TRANSACTION_DEPOSIT_CENTS = 4000000L;
    public static final int MAX_DAILY_DEPOSIT_FREQUENCY = 4;

}
