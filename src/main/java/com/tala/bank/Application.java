package com.tala.bank;

import com.tala.bank.model.AccountRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by kevin on 8/31/2016.
 */
@SpringBootApplication(scanBasePackages = {"com.tala.bank"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
