# README #

### What is this repository for? ###

* The goal of this mini project is to write a simple micro web service to mimic a "Bank Account". Through this web service, one can query about the balance, deposit money, and withdraw money.
* Version 1.0-SNAPSHOT

### How do I get set up? ###
* Java 1.7 and above
* This is a Spring boot project with gradle. You need to have groovy installed.
* The project uses an embedded H2 database

### How do I run the app? ###
* gradlew clean bootRun

### Using curl to hit the rest endpoints ###

* Deposit
curl -H "Content-Type: application/json" -X POST -d "{\"amount\":10000}" http://localhost:9000/rest/account/deposit

* Withdrawal
curl -H "Content-Type: application/json" -X POST -d "{\"amount\":5000}" http://localhost:9000/rest/account/withdrawal

* Check balance
curl http://localhost:9000/rest/account/balance

### How do I run tests? ###
* gradlew test

Test reports will be generated here:
PROJECT_ROOT\build\reports\tests

Test coverage reports will be generated here:
PROJECT_ROOT\build\reports\coverage

At the moment the test coverage is at 84%

### Who do I talk to? ###
* oti.kevin@gmail.com